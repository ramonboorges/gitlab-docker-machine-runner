variable "vpc_id" {
  type        = string
  description = "ID da VPC onde o SG será criado"
}

variable "sg_data" {
  type = map(object({
    tags = map(string)
  }))
  description = "Informações básicas p/ criar SG"
}

variable "sg_rules" {
  type = map(object({
    security_group = string
    type           = string
    from_port      = number
    to_port        = number
    protocol       = string
    cidr_block     = list(string)
    description    = string
  }))
  description = "Regras do security group"
}