resource "aws_security_group" "sg" {
  for_each    = var.sg_data
  vpc_id      = var.vpc_id
  name        = "sg_${each.key}"
  description = "Security Group - ${each.key}"

  tags = each.value["tags"]
}