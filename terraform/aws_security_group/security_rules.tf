resource "aws_security_group_rule" "example" {
  for_each          = var.sg_rules
  type              = each.value["type"]
  from_port         = each.value["from_port"]
  to_port           = each.value["to_port"]
  protocol          = each.value["protocol"]
  cidr_blocks       = each.value["cidr_block"]
  security_group_id = resource.aws_security_group.sg[each.value["security_group"]].id
  description       = each.value["description"]
}