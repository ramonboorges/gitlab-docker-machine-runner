output "sg_ids" {
  description = "Relação entre SG name e ID para uso fora do módulo"
  value = zipmap([
    for sec_group in resource.aws_security_group.sg :
    sec_group.name
    ], [
    for sec_group in resource.aws_security_group.sg :
    sec_group.id
  ])
}