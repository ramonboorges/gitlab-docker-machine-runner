resource "aws_route_table" "gitlab-route-table" {
  vpc_id = var.vpc_id

  dynamic "route" {
    for_each = var.routes
    content {
      cidr_block = route.value["cidr"]
      gateway_id = route.value["role"] == "igw" ? var.igw_id : null
    }
  }

  tags = var.tags
}

resource "aws_route_table_association" "route_and_subnet" {
  subnet_id      = var.subnet_id
  route_table_id = resource.aws_route_table.gitlab-route-table.id
}