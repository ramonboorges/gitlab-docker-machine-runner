variable "vpc_id" {
  type        = string
  description = "ID da VPC onde a route table será criada"
}

variable "subnet_id" {
  type        = string
  description = "ID da Subnet onde a table será anexada"
}

variable "igw_id" {
  type        = string
  description = "ID do internet gateway"
}

variable "routes" {
  type = map(object({
    cidr = string
    role = string
  }))
}

variable "tags" {
  type        = map(string)
  description = "Tags da subnet"
}