variable "region" {
  description = "Region onde as máquinas serão criadas"
  type        = string
  default     = "eu-central-1"
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "vpc_tags" {
  type        = map(string)
  description = "Tags da VPC"
  default = {
    Name = "GitLab-VPC"
  }
}

variable "subnet_availability_zone" {
  type        = string
  description = "AZ da subnet"
  default     = "eu-central-1a"
}

variable "subnet_cidr_block" {
  type        = string
  description = "Range de IPs da subnet criada"
  default     = "10.0.0.0/24"
}

variable "subnet_tags" {
  type        = map(string)
  description = "Tags da subnet"
  default = {
    "Name" = "GitLab-Subnet-eu-central-1a"
  }
}

variable "internet_gateway_tags" {
  type        = map(string)
  description = "Tags da subnet"
  default = {
    "Name" = "GitLab-Internet-Gateway"
  }
}

variable "routes" {
  type = map(object({
    cidr = string
    role = string
  }))
  default = {
    "route_1" = {
      cidr = "0.0.0.0/0"
      role = "igw"
    }
  }
}

variable "route_table_tags" {
  type        = map(string)
  description = "Tags da subnet"
  default = {
    "Name" = "GitLab-Route-Table"
  }
}

variable "sg_data" {
  description = "Informações básicas p/ criar SG"
  default = {
    "gitlab_docker_runner_operator" = {
      tags = {
        "Name"     = "gitlab_docker_runner_operator"
        "Engineer" = "Ramon Borges"
        "Lab"      = "GitLab Docker+Machine Runner"
      }
    }
  }
}

variable "sg_rules" {

  default = {
    "ssh_0" = {
      security_group = "gitlab_docker_runner_operator"
      type           = "ingress"
      from_port      = "22"
      to_port        = "22"
      protocol       = "tcp"
      cidr_block     = ["0.0.0.0/0"]
      description    = "SSH p/ todos"
    }
  }
  description = "Regras do security group"
}

variable "key_name" {
  description = "Nome do par de chaves que será criado"
  type        = string
  default     = "Gitlab-Runner-Operator-Key"
}

variable "instance_sec_group" {
  description = "Nome do security group que será anexado na instância"
  type        = string
  default     = "sg_gitlab_docker_runner_operator"
}

variable "instance_associate_public_ip_address" {
  description = "Associar IP público a instância"
  type        = bool
  default     = true
}

variable "instance_tags" {
  description = "Tags da instância EC2"
  type        = map(string)
  default = {
    "Name" = "Gitlab-Runner-Operator"
  }
}