output "vpc_id" {
  value       = resource.aws_vpc.gitlab.id
  description = "ID da VPC para uso em outros módulos"
}