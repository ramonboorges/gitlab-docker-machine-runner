variable "cidr_block" {
  type        = string
  description = "Bloco CIDR dessa VPC"
}

variable "enable_dns_support" {
  type        = bool
  description = "Habilitar suporte para DNS"
  default     = true
}

variable "enable_dns_hostnames" {
  type        = bool
  description = "Habilitar hostname para instâncias"
  default     = true
}

variable "tags" {
  type        = map(string)
  description = "Map das tags da VPC"
}