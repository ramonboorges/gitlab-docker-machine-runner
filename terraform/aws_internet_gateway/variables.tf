variable "vpc_id" {
  type        = string
  description = "ID da VPC onde o internet gateway será criado"
  default     = ""
}

variable "tags" {
  type        = map(string)
  description = "Tags do internet gateway"
}