output "igw_id" {
  value       = resource.aws_internet_gateway.igw.id
  description = "ID do Internet Gateway para uso fora do módulo"
}