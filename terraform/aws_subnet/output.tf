output "subnet_id" {
  value       = resource.aws_subnet.gitlab.id
  description = "ID da subnet para uso fora do módulo"
}