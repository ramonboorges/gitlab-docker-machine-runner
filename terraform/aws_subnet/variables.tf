variable "vpc_id" {
  type        = string
  description = "ID da VPC onde a subnet será criada"
}

variable "availability_zone" {
  type        = string
  description = "Availability zone onde a subnet será criada"
}

variable "cidr_block" {
  type        = string
  description = "Range de IPs da subnet criada"
}

variable "map_public_ip_on_launch" {
  description = "Associar IP público automaticamente"
  type        = bool
  default     = true
}

variable "tags" {
  type        = map(string)
  description = "Tags da subnet"
}