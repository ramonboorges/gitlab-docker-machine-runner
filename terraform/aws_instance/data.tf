data "aws_ami" "ubuntu" {
  most_recent = true

  dynamic "filter" {
    for_each = var.filters
    content {
      name   = filter.value["name"]
      values = filter.value["values"]
    }
  }

  owners = var.owners # Canonical
}