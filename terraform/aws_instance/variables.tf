variable "owners" {
  description = "Done do AMI que será usado"
  type        = list(string)
  default     = ["099720109477"]
}

variable "filters" {
  description = "Filtros para buscar AMI"
  type = map(object({
    name   = string
    values = list(string)
  }))
  default = {
    ami_filter_01 = {
      name   = "name"
      values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    },
    ami_filter_02 = {
      name   = "virtualization-type"
      values = ["hvm"]
    }
  }
}

variable "instance_type" {
  description = "Flavor da instância que será criada"
  type        = string
  default     = "t2.micro"
}

variable "sec_group_list" {
  description = "Lista de security groups p/ associação"
  type        = map(string)
}

variable "sec_group" {
  description = "Security group que será anexado a EC2"
  type        = string
}

variable "subnet_id" {
  description = "O ID da subnet onde a instância será criada"
  type        = string
}

variable "associate_public_ip_address" {
  description = "Associar IP público a instância"
  type        = bool
  default     = false
}

variable "key_name" {
  description = "Nome do par de chaves que será usado na instância"
  type        = string
}

variable "tags" {
  description = "Tags da instância EC2"
  type        = map(string)
}