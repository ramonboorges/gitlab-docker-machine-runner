resource "aws_instance" "gitlab-runner-operator" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type

  subnet_id = var.subnet_id

  associate_public_ip_address = var.associate_public_ip_address

  vpc_security_group_ids = [var.sec_group_list[var.sec_group]]

  key_name = var.key_name

  tags = var.tags
}