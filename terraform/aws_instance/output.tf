output "public_ip" {
  description = "IP público da instância para uso fora do módulo"
  value       = aws_instance.gitlab-runner-operator.public_ip
}