module "aws_vpc" {
  source     = "./aws_vpc"
  cidr_block = var.vpc_cidr_block
  tags       = var.vpc_tags
}

module "aws_subnet" {
  source            = "./aws_subnet"
  vpc_id            = module.aws_vpc.vpc_id
  availability_zone = var.subnet_availability_zone
  cidr_block        = var.subnet_cidr_block
  tags              = var.subnet_tags
}

module "aws_internet_gateway" {
  source = "./aws_internet_gateway"
  vpc_id = module.aws_vpc.vpc_id
  tags   = var.internet_gateway_tags
}

module "aws_route_table" {
  source    = "./aws_route_table"
  vpc_id    = module.aws_vpc.vpc_id
  subnet_id = module.aws_subnet.subnet_id
  igw_id    = module.aws_internet_gateway.igw_id
  routes    = var.routes
  tags      = var.route_table_tags
}

module "aws_security_group" {
  source   = "./aws_security_group"
  vpc_id   = module.aws_vpc.vpc_id
  sg_data  = var.sg_data
  sg_rules = var.sg_rules
}

module "aws_key_pair" {
  source     = "./aws_key_pair"
  key_name   = var.key_name
  public_key = file("../assets/keys/docker_machine_operator.pub")
}

module "aws_instance" {
  source                      = "./aws_instance"
  subnet_id                   = module.aws_subnet.subnet_id
  sec_group_list              = module.aws_security_group.sg_ids
  sec_group                   = var.instance_sec_group
  associate_public_ip_address = var.instance_associate_public_ip_address
  key_name                    = var.key_name
  tags                        = var.instance_tags

  depends_on = [
    module.aws_key_pair
  ]
}

output "vpc_id" {
  value = module.aws_vpc.vpc_id
}

output "subnet_id" {
  value = module.aws_subnet.subnet_id
}

output "operator_public_ip" {
  value = module.aws_instance.public_ip
}