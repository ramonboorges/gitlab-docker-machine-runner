variable "key_name" {
  description = "Nome do par de chaves que será criado"
  type        = string
}
variable "public_key" {
  description = "Public key p/ gerar par de chaves"
  type        = string
}